<?php declare(strict_types=1);

/**
 * Primary action called from the admin menu.
 *
 * Fill context[members] with the latest members who have been active and
 * have a website url or a signature
 *
 * Fill context[posts] with posts that have links
 *
 * Action: searchSpamLinks
 *
 * @return void
 */
function searchSpamLinks() {
    global $scripturl, $txt, $context, $sourcedir;
    global $smcFunc;

    // echo('<pre>'.htmlentities(print_r($_REQUEST, true)).'</pre>');

    validateSession();
    if (!empty($_POST))
        checkSession('post');

    loadTemplate('SearchSpamLinks');
    $page = 1;
    $page_count = 50;
    if (($_REQUEST['sa'] ?? '') == 'members') {
        $page = $_REQUEST['page'] ?? 1;
    }
    $context['page']['members'] = $page;
    $request = $smcFunc['db_query']('', '
        SELECT
            *
        FROM
            {db_prefix}members
        WHERE
            website_url <> \'\' OR
            signature <> \'\'
        ORDER BY
            last_login DESC
        LIMIT
            '.$page_count.' OFFSET '.($page - 1) * $page_count .'
    ',[
    ]);
    $context['members'] = [];
    while($row = $smcFunc['db_fetch_assoc']($request)) {
        // print_r($row);
        $context['members'][] = [
            'id' => $row['id_member'],
            'username' => htmlentities($row['member_name']),
            'email' => htmlentities($row['email_address']),
            'posts' => $row['posts'],
            'last_login' => date('Y-m-d H:i', (int) $row['last_login']),
            'website' => htmlentities($row['website_url']),
            'signature' => htmlentities($row['signature']),
        ];
    }
    $smcFunc['db_free_result']($request);

    $page_count = 50;
    $last_poster_time = null;
    if (($_REQUEST['sa'] ?? '') == 'posts') {
        // echo('<pre>'.htmlentities(print_r($_REQUEST, true)).'</pre>');
        $context['page']['posts'] = $_REQUEST['page'] ?? null;
        $last_poster_time = $_REQUEST['time'] ?? null;
    }
    $request = $smcFunc['db_query']('', '
        SELECT
            *
        FROM
            {db_prefix}messages
        '.($last_poster_time === null ? '' : '
        WHERE
            poster_time < {int:time}').'
        ORDER BY poster_time DESC
    ',['time' => $last_poster_time]);
    $context['posts'] = [];
    $last_poster_time = null;
    $i = 0;
    while($i < $page_count && $row = $smcFunc['db_fetch_assoc']($request)) {
        // echo('<pre>'.htmlentities(print_r($row, true)).'</pre>');
        $links = extractLinks($row['body']);
        if (count($links) == 0) {
            continue;
        }
        $i += 1;
        $last_poster_time = $row['poster_time'];
        $context['posts'][] = [
            'member_id' => $row['id_member'],
            'username' => $row['poster_name'],
            'post_id' => $row['id_msg'], // TODO: or id_msg_modified?
            'title' => htmlentities($row['subject']),
            'datetime' => date('Y-m-d H:i', (int) $row['poster_time']),
            'links' => $links,
        ];
    }
    $context['last_poster_time'] = $i == $page_count ? $last_poster_time : null;
    $smcFunc['db_free_result']($request);
}

/**
 * The smf parse_bbc converts the bbcode to html and then we extract all hrefs
 */
function extractLinks($body) {
    // echo('<pre>'.print_r(htmlentities(parse_bbc($body)), true).'</pre>');
    $matches = null;
    if (preg_match_all('/<a.*?href="(.+?)".*?>(.*?)<\/a>/', parse_bbc($body), $matches)) {
        // echo('<pre>m '.print_r($matches, true).'</pre>');
        $result = [];
        foreach (array_map(null, $matches[1], $matches[2]) as [$url, $label]) {
            $result[] = [htmlentities($url), htmlentities($label)];
        }
        return $result;
    }

    return [];
}
