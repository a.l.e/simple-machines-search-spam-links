<?php declare(strict_types=1);

function template_main()
{
    global $context, $settings, $scripturl, $txt;

    $get_pagination = function() use ($scripturl, $context) {
        $page = $context['page']['members'] ?? 1;
        $previous = $page <= 1 ? 1 : $page - 1;
        $next = $page + 1;
        return <<<EOT
            <div class="pagesection">
                <div class="pagelinks">
                    <span class="pages">Page</span>
                        <a class="nav_page" href="$scripturl?action=searchSpamLinks;sa=members;page=$previous">
                            <span class="main_icons previous_page"></span>
                        </a>
                        <span class="current_page">$page</span>
                    <a class="nav_page" href="$scripturl?action=searchSpamLinks;sa=members;page=$next">
                        <span class="main_icons next_page"></span>
                    </a>
                </div>
            </div>
        EOT;
    };

    $pagination = $get_pagination('members');
    $output = <<<EOT
    <div class="main_section">
        <div class="pagesection">
        </div>
        <div class="cat_bar">
            <h3 class="catbg">
            Show profile links for active users
            </h3>
        </div>
        <div id="mlist">
            $pagination
            <table class="table_grid">
                <thead>
                    <tr class="title_bar">
                        <th scope="col" class="lefttext" style="width:16em;">
                            Username
                        </th>
                        <th scope="col" class="lefttext" style="width:16em;">
                            Last Online
                        </th>
                        <th scope="col" class="lefttext" style="width:6em;">
                            Posts
                        </th>
                        <th scope="col" class="lefttext">
                           Links
                        </th>
                    </tr>
                <thead>
                <tbody>
    EOT;
    foreach ($context['members'] as $member) {
        // print_r($member);
        $output .= <<<EOT
                    <tr class="windowbg" style="vertical-align: top;">
                        <td class="lefttext">
                            <a href="$scripturl?action=profile;u={$member['id']}">{$member['username']}</a><br>
                            {$member['email']}

                            </td>
                        <td>{$member['last_login']}</td>
                        <td>{$member['posts']}</td>
                        <td>&#127760; {$member['website']}<br>
                        &#9998; {$member['signature']}</td> </tr>
        EOT;
    }
    if (empty($context['members'])) {
        $output .= <<<EOT
                    <tr class="windowbg">
                        <td colspan="4" class="lefttext">
                            {$txt['search_no_results']}
                        </td>
                    </tr>
        EOT;
    }
    $output .= <<<EOT
                </tbody>
            </table>
            $pagination
        </div>
        EOT;

    $get_pagination = function() use ($scripturl, $context) {
        $page = $context['page']['posts'] ?? 1;
        $time = $context['last_poster_time'];
        $next = $page + 1;
        $output = <<<EOT
            <div class="pagesection">
                <div class="pagelinks">
                    <span class="pages">Page</span>
            EOT;
        if ($page > 1) {
            $output .= <<<EOT
                        <a class="nav_page" href="$scripturl?action=searchSpamLinks;sa=posts;page=1">
                        1
                        </a>
                EOT;
        }
        if ($page > 2) {
            $output .= <<<EOT
                        <span class="expand_pages">...</a>
                EOT;
        }
        $output .= <<<EOT
                        <span class="current_page">$page</span>
            EOT;
        if (isset($time)) {
            $output .= <<<EOT
                    <a class="nav_page" href="$scripturl?action=searchSpamLinks;sa=posts;time=$time;page=$next">
                        <span class="main_icons next_page"></span>
                    </a>
                EOT;
        }
        $output .= <<<EOT
                </div>
            </div>
            EOT;
        return $output;
    };

    $pagination = $get_pagination();
    $output .= <<<EOT
        <div class="cat_bar">
            <h3 class="catbg">
            Show links in the latest posts
            </h3>
        </div>
        <div id="mlist">
            $pagination
            <table class="table_grid">
                <thead>
                    <tr class="title_bar">
                        <th scope="col" class="lefttext" style="width:16em;">
                            Username
                        </th>
                        <th scope="col" class="lefttext" style="width:16em;">
                            Post Title
                        </th>
                        <th scope="col" class="lefttext">
                           Links
                        </th>
                    </tr>
                <thead>
                <tbody>
    EOT;
    // echo('<pre>'.print_r($context['posts'], true).'</pre>');
    foreach ($context['posts'] as $post) {
        // echo('<pre>'.print_r($post['links'], true).'</pre>');
        $links = '<ul><li>' . implode('</li><li>', array_map(fn($l) => "[{$l[1]}]({$l[0]})", $post['links'])).'</li></ul>';
        $output .= <<<EOT
                    <tr class="windowbg" style="vertical-align: top;">
                        <td class="lefttext">
                            <a href="$scripturl?action=profile;u={$post['member_id']}">{$post['username']}</a><br>

                            </td>
                        <td><a href="$scripturl?msg={$post['post_id']}">{$post['title']}</a><br>
                        {$post['datetime']}
                        </td>
                        <td>{$links}</td>
                    </tr>
        EOT;
    }
    if (empty($context['posts'])) {
        $output .= <<<EOT
                    <tr class="windowbg">
                        <td colspan="3" class="lefttext">
                            {$txt['search_no_results']}
                        </td>
                    </tr>
        EOT;
    }
    $output .= <<<EOT
                </tbody>
            </table>
            $pagination
        </div>
    </div>
    EOT;
    echo($output);
}
