# Search Spam Links

Search through posts, signatures and website and find spammers.

![Screenshot](SearchSpamLinks.png)

## Details

- The mod attaches itself to the Members admin menu.
- It lists the first 50 the users that have a website or signature set, sorted by the latest login (newest first).
- For each listed user, provide a link to the member management page.
- It also lists the last 50 posts that contain links.
- Currently, there are no changes to the database.
- It should cleanly install and uninstall.

## Todo

- Mark user as trusted / spammers (needs a database table? can be done with json?)
- Use cog (https://nedbatchelder.com/code/cog/) for filling the xml file.
- Go through <https://wiki.simplemachines.org/smf/Customization_approval_guidelines>
  - ingore tabs vs spaces
  - ignore { on their line
  - fix translations

## Development

## Creating the package

`package.py`:

- copies _specific_ files from smf's `Sources` and `Themes` into the package folder.
- converts the markdown README into the bbcode one.
- adds _specific_ files to the .zip package

### Creating the readme file

Simple Machines mods _need_ a readme.txt with bbcode (but this readme file is in markdown format).

I have a `md2bbcode.py` script that can do the conversion.

### Creating the zip file

```
rm SearchSpamLinks.zip && zip -r SearchSpamLinks.zip SearchSpamLinks -x "SearchSpamLinks/.*.swp"
```

