#!/usr/bin/env python3
"""
Move files from SMF Sources and Themes to the folders in Packages,
then create the zip file in Packages/ that can be installed.

You need to run this from the Mod's folder in Packages/.
Selected content from the Mod's folder will be added to a new zip file
in the Packages/ folder.
"""
from pathlib import Path
import shutil
import zipfile

import md2bbcode

SOURCE_FILES = ['SearchSpamLinks.php', 'SearchSpamLinksHooks.php']
THEME_FILES = ['SearchSpamLinks.template.php']
ROOT_FILES = ['readme.txt', 'package-info.xml']
PACKAGE_NAME = 'SearchSpamLinks'

def main():
    package_path = Path.cwd()
    if package_path.name != PACKAGE_NAME:
        print('This script must be run from the mod folder in the smf Packages/.')

    packages_path = package_path.parent
    if packages_path.name != 'Packages':
        print(f'The {PACKAGE_NAME} folder is not in the smf Packages/ folder.')
        return

    sources_path = packages_path.parent / 'Sources'
    if not sources_path.exists():
        print('Could not find Sources/ in the parent folder.')
    target_sources_path = package_path / 'Sources'
    if not target_sources_path.exists():
        print(f'Could not find Sources/ in the {PACKAGE_NAME}/ folder.')
        return

    themes_path = packages_path.parent / 'Themes' / 'default'
    if not themes_path.exists():
        print('Could not find Themes/default/ in the parent folder.')
        return
    target_themes_path = package_path / 'Themes' / 'default'
    if not target_themes_path.exists():
        print(f'Could not find Themes/default in the {PACKAGE_NAME}/ folder.')
        return

    for filename in SOURCE_FILES:
        path = sources_path / filename
        # print(path)
        if path.exists():
            shutil.copy(path, target_sources_path / filename)
            # path.rename(target_sources_path / filename)
    for filename in THEME_FILES:
        path = themes_path / filename
        # print(path)
        if path.exists():
            shutil.copy(path, target_themes_path / filename)

    # TODO: convert the readme file to bbcode
    md2bbcode.convert((package_path / 'README.md').open('r'), (package_path / 'readme.txt').open('w'))

    with zipfile.ZipFile((package_path / PACKAGE_NAME).with_suffix('.zip') , 'w', zipfile.ZIP_DEFLATED) as zipf:
        for entry in SOURCE_FILES:
            zipf.write((target_sources_path / entry).relative_to(package_path))
        for entry in THEME_FILES:
            zipf.write((target_themes_path / entry).relative_to(package_path))
        for entry in ROOT_FILES:
            zipf.write((package_path / entry).relative_to(package_path))

if __name__ == '__main__':
    main()
