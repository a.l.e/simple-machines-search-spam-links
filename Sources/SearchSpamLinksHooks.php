<?php declare(strict_types=1);

/**
 * Hook function - Add the attachment browser buttons to the main menu.
 *
 * Hook: integrate_menu_buttons
 *
 * @param array $menu
 * @return null
 *
 */
function searchSpamLinksAdminMenu(array &$menu) {
    global $scripturl, $txt;
    // loadLanguage('SearchSpamLinks'); // TODO: add the translations
    if (!allowedTo('member_admin')) {
        return;
    }
    $title = $txt['searchSpamLinksTitle'] ?? 'Search Spam Links';
    // print_r($buttons['mlist']);
    if (array_key_exists('mlist', $menu)) {
        $menu['mlist']['sub_buttons']['searchSpamLinks'] = [
            'title' => $title,
            'href' => $scripturl . '?action=searchSpamLinks',
            'show' => true,
        ];
    }
}

/**
 *
 * Hook function - Add admin menu functions.
 *
 * Hook: integrate_admin_areas
 *
 * @param array $area
 * @return null
 *
 */
function searchSpamLinksAdminArea(array &$area) {
    global $txt;
    // loadLanguage('AttachmentBrowser');
    $title = $txt['searchSpamLinksTitle'] ?? 'Search Spam Links';

    // Add to the main menu
    $area['members']['areas']['searchSpamLinksAdmin'] = [
        'label' => $title,
        'file' => 'SearchSpamLinks.php',
        'function' => 'searchSpamLinks',
        'icon' => 'members',
        'permission' => 'manage_members',
    ];
}

/**
 * Hook function - Add the Attachment Browser action to the main action array in index.php.
 *
 * Hook: integrate_actions
 *
 * @param array $actionArray
 * @return null
 *
 */
function searchSpamLinksActions(&$action_array) {
    $action_array['searchSpamLinks'] = ['SearchSpamLinks.php', 'searchSpamLinks'];
}

/**
 * Hook function - If you're the current action, well, make yourself the current action... 
 *
 * Hook: integrate_current_action
 *
 * @param string $current_action
 * @return null
 *
 */
function searchSpamLinkCurrentAction(&$current_action) {
    global $context;

    if (isset($context['current_action']) && ($context['current_action'] == 'searchSpamLinks'))
        $current_action = 'mlist';
}
